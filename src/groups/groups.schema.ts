import Joi from 'joi';
import { IGroup } from './types/groups.interface';

//when create Group, it should be IGroup without id
export const groupCreateSchema = Joi.object<Omit<IGroup, 'id'>>({
  name: Joi.string().required(), // required - must be
});

//when update Group, it should be IGroup optionals
export const groupUpdateSchema = Joi.object<Partial<IGroup>>({
  name: Joi.string().optional(), // optional - be or not
});
