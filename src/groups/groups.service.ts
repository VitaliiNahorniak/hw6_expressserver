import { DeleteResult, UpdateResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Group } from './entities/group.entity';
import { IGroup } from './types/groups.interface';

const groupsRepository = AppDataSource.getRepository(Group);

export const getAllGroups = async (): Promise<Group[]> => {
  const groups = await groupsRepository
    .createQueryBuilder('group')
    //.select(['group.id as id', 'group.name as name'])
    .leftJoinAndSelect('group.students', 'students')
    .getMany();

  if (!groups) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return groups;
};

export const getGroupById = async (id: string): Promise<Group> => {
  const group = await groupsRepository
    .createQueryBuilder('group')
    //.select(['group.id as id', 'group.name as name'])
    .leftJoinAndSelect('group.students', 'students')
    .where('group.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return group;
};

export const createGroup = async (
  createGroupSchema: Omit<IGroup, 'id'>,
): Promise<Group> => {
  const group = await groupsRepository.findOne({
    where: {
      name: createGroupSchema.name,
    },
  });

  if (group) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this name already exists',
    );
  }

  return groupsRepository.save(createGroupSchema);
};

export const updateGroupById = async (
  groupId: string,
  updateGroupSchema: Partial<IGroup>,
): Promise<UpdateResult> => {
  const result = await groupsRepository.update(groupId, updateGroupSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return result;
};

export const deleteGroupById = async (
  groupId: string,
): Promise<DeleteResult> => {
  const result = await groupsRepository.delete(groupId);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return result;
};
