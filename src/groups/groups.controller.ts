import { Request, Response } from 'express';
import * as groupsService from './groups.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupUpdateRequest } from './types/group-update-request.interface';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllGroups = async (request: Request, response: Response) => {
  const groups = await groupsService.getAllGroups();
  response.status(HttpStatuses.OK).json(groups);
};

export const getGroupById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.getGroupById(id);
  response.status(HttpStatuses.OK).json(group);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = await groupsService.createGroup(request.body);
  response.status(HttpStatuses.CREATED).json(group);
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.updateGroupById(id, request.body);
  response.status(HttpStatuses.ACCEPTED).json(group);
};

export const deleteGroupById = async (
  request: Request<{ id: string }>, //request: Request
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.deleteGroupById(id);
  response.status(HttpStatuses.OK).json(group);
};
