import Joi from 'joi';
import { ILectorCourse } from './type/lector_course.interface';

export const lectorCourseCreateSchema = Joi.object<Omit<ILectorCourse, 'id'>>({
  lectorId: Joi.number().required(), // required - must be
  courseId: Joi.number().required(),
});
