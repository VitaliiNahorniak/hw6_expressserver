import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ILectorCourse } from './lector_course.interface';

export interface ILectorCourseCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ILectorCourse, 'id'>;
}
