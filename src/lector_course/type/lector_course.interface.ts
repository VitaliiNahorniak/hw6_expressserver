export interface ILectorCourse {
  id: string;
  lectorId: number;
  courseId: number;
}
