import { Request, Response, NextFunction } from 'express';
import HttpException from '../exceptions/http-exception';
import { HttpStatuses } from '../enums/http-statuses.enum';

//middleware
const exceptionFilter = (
  error: HttpException,
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const status = error.status || HttpStatuses.INTERNAL_SERVER_ERROR;
  const message = error.message || 'Something went wrong';
  response.status(status).send({ status, message });
  next();
};

export default exceptionFilter;

//Use in the end of APP
