import Joi from 'joi';
import { ICourse } from './type/course.interface';

//when create course, it should be ICourse without id
export const courseCreateSchema = Joi.object<Omit<ICourse, 'id'>>({
  name: Joi.string().required(), // required - must be
  description: Joi.string().required(),
  hours: Joi.number().required(),
});

//when update course, it should be ICourse optionals
export const courseUpdateSchema = Joi.object<Partial<ICourse>>({
  name: Joi.string().optional(), // optional - be or not
  description: Joi.string().optional(),
  hours: Joi.number().optional(),
});
