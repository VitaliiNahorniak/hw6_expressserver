import { HoursOfCourse } from '../enums/hours.enum';

export interface ICourse {
  id: string;
  name: string;
  description: string;
  hours: HoursOfCourse;
}
