import { Request, Response } from 'express';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import * as coursesService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ICourseCreateRequest } from './type/course-create-request.interface';
import { ICourseUpdateRequest } from './type/course-update-request.interface';

export const getAllCourses = async (request: Request, response: Response) => {
  const courses = await coursesService.getAllCourses();
  response.status(HttpStatuses.OK).json(courses);
};

export const getCourseById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.getCourseById(id);
  response.status(HttpStatuses.OK).json(course);
};

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response,
) => {
  const course = await coursesService.createCourse(request.body);
  response.status(HttpStatuses.CREATED).json(course);
};

export const updateCourseById = async (
  request: ValidatedRequest<ICourseUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.updateCourseById(id, request.body);
  response.status(HttpStatuses.ACCEPTED).json(course);
};

export const deleteCourseById = async (
  request: Request<{ id: string }>, //request: Request
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.deleteCourseById(id);
  response.status(HttpStatuses.OK).json(course);
};

export const getCourseMarksById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const courseWithMarks = await coursesService.getCourseMarksById(id);
  response.status(HttpStatuses.OK).json(courseWithMarks);
};
