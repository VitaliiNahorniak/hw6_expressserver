import { Router } from 'express';
import * as coursesController from './courses.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { idParamsSchema } from '../application/schemas/id-param.schema';
import { courseCreateSchema, courseUpdateSchema } from './courses.schema';

const router = Router();

router.get('/', controllerWrapper(coursesController.getAllCourses));

router.get(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(coursesController.getCourseById),
);

router.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse),
);

router.patch(
  '/:id',
  validator.params(idParamsSchema),
  validator.body(courseUpdateSchema),
  controllerWrapper(coursesController.updateCourseById),
);

router.delete(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(coursesController.deleteCourseById),
);

router.get(
  '/:id/marks',
  validator.params(idParamsSchema),
  controllerWrapper(coursesController.getCourseMarksById),
);

export default router;
