import { DeleteResult, UpdateResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from './entities/course.entity';
import { ICourse } from './type/course.interface';

const coursesRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async (): Promise<Course[]> => {
  return await coursesRepository.find({});
};

export const getCourseById = async (id: string): Promise<Course> => {
  const course = await coursesRepository
    .createQueryBuilder('courses')
    .leftJoinAndSelect('courses.lectors', 'lector')
    .where('courses.id = :id', { id })
    .getOne();

  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  return course;
};

export const createCourse = async (
  createCourseSchema: Omit<ICourse, 'id'>,
): Promise<Course> => {
  const course = await coursesRepository.findOne({
    where: {
      name: createCourseSchema.name,
    },
  });

  if (course) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Course with this name already exists',
    );
  }

  return coursesRepository.save(createCourseSchema);
};

export const updateCourseById = async (
  courseId: string,
  updateCourseSchema: Partial<ICourse>,
): Promise<UpdateResult> => {
  const result = await coursesRepository.update(courseId, updateCourseSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  return result;
};

export const deleteCourseById = async (
  courseId: string,
): Promise<DeleteResult> => {
  const result = await coursesRepository.delete(courseId);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  return result;
};

export const getCourseMarksById = async (id: string): Promise<Course> => {
  const courseWithMarks = await coursesRepository
    .createQueryBuilder('course')
    .select(['course.id', 'course.name'])
    .leftJoin('course.marks', 'mark')
    .addSelect([
      'mark.mark',
      'mark.studentId',
      'mark.lectorId',
      'mark.student',
      'mark.lector',
    ])
    .leftJoin('mark.student', 'student')
    .addSelect(['student.name'])
    .leftJoin('mark.lector', 'lector')
    .addSelect(['lector.name'])
    .where('course.id = :id', { id })
    .getOne();

  if (!courseWithMarks) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  return courseWithMarks;
};
