import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRelativeForMarksTable1688483346050
  implements MigrationInterface
{
  name = 'AddRelativeForMarksTable1688483346050';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "PK_9c0e67a93db6e3fb479ba848b30"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "PK_8be1483bd46496120ef662ecc8a" PRIMARY KEY ("id", "student_id", "lector_id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "PK_8be1483bd46496120ef662ecc8a"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "PK_e9ed8cc1d0bfef956c98d61506f" PRIMARY KEY ("id", "lector_id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "PK_e9ed8cc1d0bfef956c98d61506f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "PK_051deeb008f7449216d568872c6" PRIMARY KEY ("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_5226e1592e6291dbe7a07640346" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "FK_5226e1592e6291dbe7a07640346"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "PK_051deeb008f7449216d568872c6"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "PK_e9ed8cc1d0bfef956c98d61506f" PRIMARY KEY ("id", "lector_id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "PK_e9ed8cc1d0bfef956c98d61506f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "PK_8be1483bd46496120ef662ecc8a" PRIMARY KEY ("id", "student_id", "lector_id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "PK_8be1483bd46496120ef662ecc8a"`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "PK_9c0e67a93db6e3fb479ba848b30" PRIMARY KEY ("id", "course_id", "student_id", "lector_id")`,
    );
  }
}
