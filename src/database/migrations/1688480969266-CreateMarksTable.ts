import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateMarksTable1688480969266 implements MigrationInterface {
  name = 'CreateMarksTable1688480969266';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "marks" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "mark" numeric NOT NULL, "course_id" integer NOT NULL, "student_id" integer NOT NULL, "lector_id" integer NOT NULL, CONSTRAINT "PK_9c0e67a93db6e3fb479ba848b30" PRIMARY KEY ("id", "course_id", "student_id", "lector_id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "marks"`);
  }
}
