import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateLectorCourseTable1688478109564 implements MigrationInterface {
    name = 'CreateLectorCourseTable1688478109564'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "lector_course" ("lector_id" integer NOT NULL, "course_id" integer NOT NULL, CONSTRAINT "PK_79cf0f064235769277ce94c75f7" PRIMARY KEY ("lector_id", "course_id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "lector_course"`);
    }

}
