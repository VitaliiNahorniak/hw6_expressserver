import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IStudent } from './types/students.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { Mark } from '../marks/entities/mark.entity';
import { IMark } from '../marks/type/mark.interface';

const studentsRepository = AppDataSource.getRepository(Student);

const marksRepository = AppDataSource.getRepository(Mark);

export const getAllStudents = async (
  name?: string | undefined,
): Promise<Student[]> => {
  //return studentsRepository.find({});

  if (name) {
    const students = await studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.imagePath as imagePath',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.name = :name', { name })
      .getRawOne();

    if (!students) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    return students;
  } else {
    return studentsRepository.find({});
  }
};

export const getStudentById = async (id: string): Promise<Student> => {
  // const student = await studentsRepository.findOne({
  //   where: {
  //     id,
  //   },
  // });

  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as imagePath',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const createStudent = async (
  createStudentSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: createStudentSchema.email,
    },
  });

  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exists',
    );
  }

  return studentsRepository.save(createStudentSchema);
};

export const updateStudentById = async (
  studentId: string,
  updateStudentSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(
    studentId,
    updateStudentSchema,
  );

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return result;
};

export const deleteStudentById = async (
  studentId: string,
): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(studentId);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return result;
};

export const addMarkToStudent = async (
  addMarkSchema: Omit<IMark, 'id'>,
): Promise<Mark> => {
  const mark = await marksRepository.findOne({
    where: {
      mark: addMarkSchema.mark,
      courseId: addMarkSchema.courseId,
      studentId: addMarkSchema.studentId,
      lectorId: addMarkSchema.lectorId,
    },
  });

  if (mark) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'This mark already exists',
    );
  }

  return marksRepository.save(addMarkSchema);
};

export const getStudentMarksById = async (id: string): Promise<Student> => {
  const studentWithMarks = await studentsRepository
    .createQueryBuilder('student')
    .select(['student.id', 'student.name', 'student.surname'])
    .leftJoin('student.marks', 'mark')
    .addSelect(['mark.mark', 'mark.courseId', 'mark.course'])
    .leftJoin('mark.course', 'course')
    .addSelect(['course.name'])
    .where('student.id = :id', { id })
    .getOne();

  if (!studentWithMarks) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return studentWithMarks;
};
