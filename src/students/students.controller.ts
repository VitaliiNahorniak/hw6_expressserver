import { Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import { IStudentAddGroupIdRequest } from './types/student-addGroupId-request.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import { IMarkCreateRequest } from '../marks/type/mark-create-request.interface';

export const getAllStudents = async (
  request: Request<{ name: string }>,
  response: Response,
) => {
  const { name } = request.query;

  if (typeof name === 'string') {
    const students = await studentsService.getAllStudents(name);
    response.status(HttpStatuses.OK).json(students);
  } else {
    const students = await studentsService.getAllStudents();
    response.status(HttpStatuses.OK).json(students);
  }
};

export const getStudentById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentById(id);
  response.status(HttpStatuses.OK).json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = await studentsService.createStudent(request.body);
  response.status(HttpStatuses.CREATED).json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentById(id, request.body);
  // response.status(HttpStatuses.NO_CONTENT).json(student);
  response.status(HttpStatuses.NO_CONTENT).json();
};

export const addGroupIdForStudentById = async (
  request: ValidatedRequest<IStudentAddGroupIdRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentById(id, request.body);
  response.status(HttpStatuses.CREATED).json(student);
};

export const deleteStudentById = async (
  request: Request<{ id: string }>, //request: Request
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);
  // response.status(HttpStatuses.NO_CONTENT).json(student);
  response.status(HttpStatuses.NO_CONTENT).json();
};

export const addMarkToStudent = async (
  request: ValidatedRequest<IMarkCreateRequest>,
  response: Response,
) => {
  const mark = await studentsService.addMarkToStudent(request.body);
  response.status(HttpStatuses.CREATED).json(mark);
};

export const getStudentMarksById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const studentWithMarks = await studentsService.getStudentMarksById(id);
  response.status(HttpStatuses.OK).json(studentWithMarks);
};
