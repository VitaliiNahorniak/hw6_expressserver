import { Router } from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  studentAddGroupIdSchema,
  studentCreateSchema,
  studentNameSchema,
  studentUpdateSchema,
} from './student.schema';
import { idParamsSchema } from '../application/schemas/id-param.schema';
import { markCreateSchema } from '../marks/mark.schema';

const router = Router();

router.get(
  '/',
  validator.query(studentNameSchema),
  controllerWrapper(studentsController.getAllStudents),
);

router.get(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(studentsController.getStudentById),
);

router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);

router.patch(
  '/:id',
  validator.params(idParamsSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
);

//Add groupId
router.patch(
  '/:id/add-group-id',
  validator.params(idParamsSchema),
  validator.body(studentAddGroupIdSchema),
  controllerWrapper(studentsController.addGroupIdForStudentById),
);

router.delete(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(studentsController.deleteStudentById),
);

router.post(
  '/add-mark',
  validator.body(markCreateSchema),
  controllerWrapper(studentsController.addMarkToStudent),
);

router.get(
  '/:id/marks',
  validator.params(idParamsSchema),
  controllerWrapper(studentsController.getStudentMarksById),
);

export default router;
