import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Index()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  age: number;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  imagePath?: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: false,
    eager: false, //automatic upload group - false. We will use for that JOIN
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @Column({
    type: 'numeric',
    nullable: true,
    name: 'group_id',
  })
  group_id: number;

  @OneToMany(() => Mark, (mark) => mark.student, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  marks: Mark[];
}
