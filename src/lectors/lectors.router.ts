import { Router } from 'express';
import * as lectorsController from './lectors.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { idParamsSchema } from '../application/schemas/id-param.schema';
import { lectorCreateSchema, lectorUpdateSchema } from './lector.schema';
import { lectorCourseCreateSchema } from '../lector_course/lector_course.schema';

const router = Router();

router.get('/', controllerWrapper(lectorsController.getAllLectors));

router.get(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(lectorsController.getLectorById),
);

router.get(
  '/:id/only-courses',
  validator.params(idParamsSchema),
  controllerWrapper(lectorsController.getLectorCoursesById),
);

router.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector),
);

router.patch(
  '/:id',
  validator.params(idParamsSchema),
  validator.body(lectorUpdateSchema),
  controllerWrapper(lectorsController.updateLectorById),
);

router.delete(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(lectorsController.deleteLectorById),
);

router.post(
  '/add-course',
  validator.body(lectorCourseCreateSchema),
  controllerWrapper(lectorsController.addCourseToLector),
);

export default router;
