import Joi from 'joi';
import { ILector } from './type/lectors.interface';

//when create lector, it should be ILector without id
export const lectorCreateSchema = Joi.object<Omit<ILector, 'id'>>({
  name: Joi.string().required(), // required - must be
  email: Joi.string().required(),
  password: Joi.string().required(),
});

//when update lector, it should be ILector optionals
export const lectorUpdateSchema = Joi.object<Partial<ILector>>({
  name: Joi.string().optional(), // optional - be or not
  email: Joi.string().optional(),
  password: Joi.string().optional(),
});
