import { Request, Response } from 'express';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import * as lectorsService from './lectors.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorCreateRequest } from './type/lector-create-request.interface';
import { ILectorUpdateRequest } from './type/lector-update-request.interface';
import { ILectorCourseCreateRequest } from '../lector_course/type/lector-course-create-request.interface';

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors();
  response.status(HttpStatuses.OK).json(lectors);
};

export const getLectorById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.getLectorById(id);
  response.status(HttpStatuses.OK).json(lector);
};

export const getLectorCoursesById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const lectorCourses = (await lectorsService.getLectorCoursesById(id)).courses;
  response.status(HttpStatuses.OK).json(lectorCourses);
};

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response,
) => {
  const lector = await lectorsService.createLector(request.body);
  response.status(HttpStatuses.CREATED).json(lector);
};

export const updateLectorById = async (
  request: ValidatedRequest<ILectorUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.updateLectorById(id, request.body);
  response.status(HttpStatuses.ACCEPTED).json(lector);
};

export const deleteLectorById = async (
  request: Request<{ id: string }>, //request: Request
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.deleteLectorById(id);
  response.status(HttpStatuses.OK).json(lector);
};

export const addCourseToLector = async (
  request: ValidatedRequest<ILectorCourseCreateRequest>,
  response: Response,
) => {
  const lectorCourse = await lectorsService.addCourseToLector(request.body);
  response.status(HttpStatuses.CREATED).json(lectorCourse);
};
