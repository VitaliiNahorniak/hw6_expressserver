import { DeleteResult, UpdateResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Lector } from './entities/lector.entity';
import { ILector } from './type/lectors.interface';
import { LectorCourse } from '../lector_course/entities/lector_course.entity';
import { ILectorCourse } from '../lector_course/type/lector_course.interface';

const lectorsRepository = AppDataSource.getRepository(Lector);

const lectorCourseRepository = AppDataSource.getRepository(LectorCourse);

export const getAllLectors = async (): Promise<Lector[]> => {
  return await lectorsRepository.find({});
};

export const getLectorById = async (id: string): Promise<Lector> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lectors')
    .leftJoinAndSelect('lectors.courses', 'course')
    .where('lectors.id = :id', { id })
    .getOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return lector;
};

export const getLectorCoursesById = async (id: string): Promise<Lector> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lectors')
    // .select(['lectors.id as id', 'lectors.courses as courses'])
    .leftJoinAndSelect('lectors.courses', 'course')
    .where('lectors.id = :id', { id })
    .getOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return lector;
};

export const createLector = async (
  createLectorSchema: Omit<ILector, 'id'>,
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: {
      name: createLectorSchema.name,
    },
  });

  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this name already exists',
    );
  }

  return lectorsRepository.save(createLectorSchema);
};

export const updateLectorById = async (
  lectorId: string,
  updateLectorSchema: Partial<ILector>,
): Promise<UpdateResult> => {
  const result = await lectorsRepository.update(lectorId, updateLectorSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return result;
};

export const deleteLectorById = async (
  lectorId: string,
): Promise<DeleteResult> => {
  const result = await lectorsRepository.delete(lectorId);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return result;
};

export const addCourseToLector = async (
  createLectorCourseSchema: Omit<ILectorCourse, 'id'>,
): Promise<LectorCourse> => {
  const lector = await lectorCourseRepository.findOne({
    where: {
      lectorId: createLectorCourseSchema.lectorId,
      courseId: createLectorCourseSchema.courseId,
    },
  });

  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this course already exists',
    );
  }

  return lectorCourseRepository.save(createLectorCourseSchema);
};
