import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ILector } from './lectors.interface';

export interface ILectorUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<ILector>;
}
