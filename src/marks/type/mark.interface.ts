export interface IMark {
  id: string;
  mark: number;
  courseId: number;
  studentId: number;
  lectorId: number;
}
