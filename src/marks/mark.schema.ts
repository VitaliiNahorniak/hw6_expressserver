import Joi from 'joi';
import { IMark } from './type/mark.interface';

export const markCreateSchema = Joi.object<Omit<IMark, 'id'>>({
  mark: Joi.number().min(0).max(100).required(), // required - must be
  courseId: Joi.number().required(),
  studentId: Joi.number().required(),
  lectorId: Joi.number().required(),
});
